# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(a, b, c)
 
  arr = [a,b,c]
   
   for i in 0..2
     for j in 0..2
         next if i == j
         for k in 0..2
            next if j == k || i == k 
             raise TriangleError.new unless (arr[i] < arr[j] + arr[k])
         end
     end
end

  # WRITE THIS CODE
  if a == b and b == c and a == c
    return :equilateral
  end
  if a == b or b == c or c == a
    return :isosceles
  end
  :scalene
end

# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
