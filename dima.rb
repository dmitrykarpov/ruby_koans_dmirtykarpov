system('cls')

def score(dice)
    count = 0
    (0..9).each do |item|
        tmp = dice.find_all do |j| 
            j == item
        end
         mod = tmp.size%3
         full = (tmp.size - mod)/3

         count += full*100*item unless 1 == item
         count += full*1000 if 1 == item

         count += 50 * mod if 5 == item
         count += 100 * mod if 1 == item
    end
    return count;
end

test = [
    [[],0],
    [[5],50],
    [[1],100],
    [[1,5,5,1],300],
    [[2,3,4,6],0],
    [[1,1,1],1000],
    [[2,2,2],200],
    [[2,5,2,2,3],250],
    [[5,5,5,5],550],
    [[1,1,1,1],1100],
    [[1,1,1,1,1],1200],
    [[1,1,1,5,1],1150]


]

test.each do |item|
   arr = item[0]
   ans = item[1] 
   res = score(arr)
   p [res == ans, ans, res, arr ]
end